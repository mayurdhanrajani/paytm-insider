package com.example.paytminsiderdemo.events

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.databinding.ListItemHorizontalEventBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.utils.loadImage

/**
 * This class implements a [RecyclerView] [ListAdapter] which uses Data Binding to present [List] data, including computing diffs between lists.
 */
class EventsListAdapter :
    ListAdapter<EventDetails, EventsListViewHolder>(
        EventsListDiffCallback()
    ) {

    /**
     * Create new [RecyclerView] item views (invoked by the layout manager).
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsListViewHolder {
        return EventsListViewHolder.from(
            parent
        )
    }

    /**
     * Replaces the contents of a view (invoked by the layout manager).
     */
    override fun onBindViewHolder(holder: EventsListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

/**
 * The EventsListViewHolder constructor takes the binding variable from the associated
 * ListItemHorizontalEvent, which gives it access to the full [EventDetails] information.
 */
class EventsListViewHolder(private val binding: ListItemHorizontalEventBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(eventDetails: EventDetails) {
        binding.data = eventDetails
        binding.ivEvent.loadImage(eventDetails.horizontalCoverImage)
    }

    companion object {
        fun from(parent: ViewGroup): EventsListViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemHorizontalEventBinding.inflate(layoutInflater, parent, false)
            return EventsListViewHolder(
                binding
            )
        }
    }

}

/**
 * Allows the [RecyclerView] to determine which items have changed when the [List] of [EventDetails] has been updated.
 */
class EventsListDiffCallback : DiffUtil.ItemCallback<EventDetails>() {

    override fun areItemsTheSame(oldItem: EventDetails, newItem: EventDetails): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EventDetails, newItem: EventDetails): Boolean {
        return oldItem == newItem
    }

}
