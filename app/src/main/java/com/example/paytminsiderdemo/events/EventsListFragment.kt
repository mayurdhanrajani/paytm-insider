package com.example.paytminsiderdemo.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.databinding.FragmentListEventsBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventCategory
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails

/**
 * Shows the list of events.
 */
class EventsListFragment : Fragment() {

    private lateinit var binding: FragmentListEventsBinding

    private lateinit var eventsListAdapter: EventsListAdapter

    private lateinit var eventCategory: EventCategory

    private lateinit var data: List<EventDetails>

    private lateinit var filterType: String

    private lateinit var toolbarTitle: String

    private var startTime: Long = 0

    private var endTime: Long = 0

    /**
     * Inflates the layout with Data Binding, sets its lifecycle owner to the [EventsListFragment] to enable DataBinding.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListEventsBinding.inflate(inflater)
        return binding.root
    }

    /**
     * Initializes the classes after view is created.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProperty()
        setupToolbar()
        initializeView()
    }

    /**
     * Set the values of the variables from arguments.
     */
    private fun setProperty() {
        arguments?.let {
            filterType = it.getString(getString(R.string.filter_type)) ?: ""
            toolbarTitle = it.getString(getString(R.string.toolbar_title)) ?: ""
            eventCategory = it.getParcelable(getString(R.string.event_category)) ?: EventCategory()
            data = it.getParcelableArrayList(getString(R.string.data)) ?: listOf()
            startTime = it.getLong(getString(R.string.start_time))
            endTime = it.getLong(getString(R.string.end_time))
        }
    }

    /**
     * Sets up the toolbar.
     */
    private fun setupToolbar() {
        (activity as AppCompatActivity).supportActionBar?.apply {
            title = toolbarTitle
        }
    }

    /**
     * Initializes the views.
     */
    private fun initializeView() {
        eventsListAdapter = EventsListAdapter()

        if (filterType == getString(R.string.category)) {
            eventsListAdapter.submitList(data.filter { it.eventCategory.name == eventCategory.name })
        } else {
            eventsListAdapter.submitList(data.filter { it.minShowStartTime in startTime..endTime })
        }

        binding.rvEvents.adapter = eventsListAdapter
    }

}