package com.example.paytminsiderdemo.utils

import android.widget.ImageView
import coil.api.load
import java.util.*

/**
 * Returns the epoch start time of the day.
 */
fun Calendar.getStartTime(day: Int): Long {
    val calendar = this
    calendar.set(
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH) + day,
        0, 0, 0
    )
    return calendar.timeInMillis.div(1000)
}

/**
 * Returns the epoch end time of the day.
 */
fun Calendar.getEndTime(day: Int): Long {
    val calendar = this
    calendar.set(
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH) + day,
        23, 59, 59
    )
    return calendar.timeInMillis.div(1000)
}

/**
 * Loads the Image in [ImageView] inside the Material CardView using Coil library.
 */
fun ImageView.loadImage(url: String) {
    this.load(url)
}