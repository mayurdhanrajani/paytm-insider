package com.example.paytminsiderdemo.utils

/**
 * Class which determine the type of events to be showed based on the Date type.
 */
enum class EventDateType {

    TODAY, TOMORROW, WEEKEND;

    override fun toString(): String {
        return super.toString().toLowerCase().capitalize()
    }

}