package com.example.paytminsiderdemo.utils

/**
 * Object which stores the Constant values used in the app.
 */
object Constants {

    /**
     * The URL which will be used to make the network requests using retrofit.
     */
    const val BASE_URL = "https://api.insider.in/"

    /**
     * The delay time in millis before navigating to home screen.
     */
    const val SPLASH_TIME: Long = 3000

}