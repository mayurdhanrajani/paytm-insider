package com.example.paytminsiderdemo.network

import com.example.paytminsiderdemo.home.model.datamodels.EventData

/**
 * Base class which represents Network response states.
 */
sealed class NetworkResponseState {

    /**
     * The request was successful.
     */
    data class Success(val data: EventData) : NetworkResponseState()

    /**
     * Error while performing a HTTP request.
     */
    data class ApiError(val code: Int, val message: String) : NetworkResponseState()

    /**
     * Network error while performing a HTTP request.
     */
    data class NetworkError(val message: String) : NetworkResponseState()

    /**
     * Unknown error.
     */
    data class UnknownError(val message: String) : NetworkResponseState()

}