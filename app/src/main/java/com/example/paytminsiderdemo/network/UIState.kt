package com.example.paytminsiderdemo.network

/**
 * Base class which represents different UI states.
 */
enum class UIState {
    LOADING, ERROR, EMPTY, SUCCESS
}