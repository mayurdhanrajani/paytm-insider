package com.example.paytminsiderdemo.network

import com.example.paytminsiderdemo.home.model.datamodels.EventData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

/**
 * Invokes the api call and returns the response state in [NetworkResponseState] sealed class.
 */
suspend fun executeCoroutine(api: suspend () -> EventData): NetworkResponseState {
    return withContext(Dispatchers.IO) {
        try {
            NetworkResponseState.Success(api.invoke())
        } catch (exception: Exception) {
            when (exception) {
                is HttpException -> NetworkResponseState.ApiError(exception.code(), exception.message())
                is IOException -> NetworkResponseState.NetworkError(exception.message.toString())
                else -> NetworkResponseState.UnknownError(exception.message.toString())
            }
        }
    }
}