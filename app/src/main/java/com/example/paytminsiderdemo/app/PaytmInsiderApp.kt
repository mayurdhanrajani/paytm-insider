package com.example.paytminsiderdemo.app

import android.app.Application
import com.example.paytminsiderdemo.di.AppComponent
import com.example.paytminsiderdemo.di.AppModule
import com.example.paytminsiderdemo.di.DaggerAppComponent
import com.example.paytminsiderdemo.utils.Constants.BASE_URL

/**
 * The first class in the project which is called when the app starts.
 */
class PaytmInsiderApp : Application() {

    private lateinit var appComponent: AppComponent

    /**
     * Builds the [AppComponent] that will be used across the whole app.
     */
    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(BASE_URL))
            .build()

    }

    /**
     * Returns the instance of [AppComponent].
     */
    fun getAppComponent(): AppComponent {
        return appComponent
    }

}