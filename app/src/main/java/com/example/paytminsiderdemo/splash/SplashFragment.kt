package com.example.paytminsiderdemo.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.utils.Constants

/**
 * Shows the app icon for some duration and navigates to Home screen.
 */
class SplashFragment : Fragment() {

    /**
     * Inflates the layout.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    /**
     * Initializes the classes after view is created.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        navigateToHomeScreen()
    }

    /**
     * Hides the toolbar.
     */
    private fun hideToolbar() {
        (activity as AppCompatActivity).supportActionBar?.apply {
            hide()
        }
    }

    /**
     * Navigates to the Home screen
     */
    private fun navigateToHomeScreen() {
        Handler().postDelayed({
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        }, Constants.SPLASH_TIME)
    }

}