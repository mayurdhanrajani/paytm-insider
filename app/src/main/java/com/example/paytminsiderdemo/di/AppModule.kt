package com.example.paytminsiderdemo.di

import androidx.lifecycle.ViewModel
import com.example.paytminsiderdemo.utils.ViewModelProviderFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Provider
import javax.inject.Singleton

/**
 * This module provides the dependencies which will be used across the whole app.
 */
@Module
class AppModule(private val BASE_URL: String) {

    /**
     * Provides the [Moshi] object with Kotlin adapter that Retrofit will be using.
     */
    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    /**
     * Provides the Retrofit instance with [MoshiConverterFactory] using builder pattern.
     */
    @Singleton
    @Provides
    fun provideRetrofit(moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()
    }


    /**
     * Simple ViewModel factory that provides the parameters respective to the ViewModel.
     */
    @Singleton
    @Provides
    fun provideViewModelFactory(providerMap: Map<Class<out ViewModel>, Provider<ViewModel>>): ViewModelProviderFactory {
        return ViewModelProviderFactory(providerMap)
    }

}