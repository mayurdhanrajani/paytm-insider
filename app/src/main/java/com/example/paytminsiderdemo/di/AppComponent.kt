package com.example.paytminsiderdemo.di

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * This component generates a container with all the dependencies provided by the [AppModule].
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun retrofit(): Retrofit

}