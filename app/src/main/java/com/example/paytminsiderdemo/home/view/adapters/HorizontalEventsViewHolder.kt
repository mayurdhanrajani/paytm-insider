package com.example.paytminsiderdemo.home.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.databinding.ListItemHorizontalEventBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.utils.loadImage

/**
 * The HorizontalEventViewHolder constructor takes the binding variable from the associated
 * ListItemHorizontalEvent, which gives it access to the full [EventDetails] information.
 */
class HorizontalEventViewHolder(private val binding: ListItemHorizontalEventBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(eventDetails: EventDetails) {
        binding.data = eventDetails
        binding.ivEvent.loadImage(eventDetails.horizontalCoverImage)
    }

    companion object {
        fun from(parent: ViewGroup): HorizontalEventViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemHorizontalEventBinding.inflate(layoutInflater, parent, false)
            return HorizontalEventViewHolder(
                binding
            )
        }
    }

}