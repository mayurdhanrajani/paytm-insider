package com.example.paytminsiderdemo.home.utils

enum class EventListItemType {
    DATE,
    HORIZONTAL_EVENT,
    VERTICAL_EVENT,
    CATEGORY
}