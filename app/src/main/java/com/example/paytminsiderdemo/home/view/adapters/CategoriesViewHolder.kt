package com.example.paytminsiderdemo.home.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.databinding.ListItemCategoryBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.home.utils.CategoryListener
import com.google.android.material.chip.Chip

/**
 * The CategoriesViewHolder constructor takes the binding variable from the associated
 * ListItemCategory, which gives it access to the full [CategoryListener] and [EventDetails] information.
 */
class CategoriesViewHolder(
    private val binding: ListItemCategoryBinding,
    private val context: Context
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(categoryListener: CategoryListener, eventDetails: List<EventDetails>) {
        binding.tvHeader.text = context.getString(R.string.categories)
        eventDetails.forEach {
            val chip = Chip(context)
            chip.text = it.eventCategory.name
            chip.setOnClickListener { _ ->
                categoryListener.onClick(it.eventCategory)
            }
            binding.cgEvents.addView(chip)
        }
    }

    companion object {
        fun from(parent: ViewGroup, context: Context): CategoriesViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemCategoryBinding.inflate(layoutInflater, parent, false)
            return CategoriesViewHolder(
                binding,
                context
            )
        }
    }

}