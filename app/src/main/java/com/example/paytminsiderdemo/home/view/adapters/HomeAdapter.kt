package com.example.paytminsiderdemo.home.view.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.home.model.datamodels.EventListSection
import com.example.paytminsiderdemo.home.utils.CategoryListener
import com.example.paytminsiderdemo.home.utils.DateListener
import com.example.paytminsiderdemo.home.utils.EventListItemType

/**
 * This class implements a [RecyclerView] [ListAdapter] which uses Data Binding to present [List] data, including computing diffs between lists.
 *
 * @param context - Provides context to access resources.
 *
 * @param dateListener - Handle click events in [DatesViewHolder].
 *
 * @param categoryListener - Handle click events in [CategoriesViewHolder].
 *
 */
class HomeAdapter(
    private val context: Context,
    private val dateListener: DateListener,
    private val categoryListener: CategoryListener
) :
    ListAdapter<EventListSection, RecyclerView.ViewHolder>(HomeDiffCallback()) {

    /**
     * Create new [RecyclerView] item views (invoked by the layout manager).
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            EventListItemType.DATE.ordinal -> DatesViewHolder.from(parent)
            EventListItemType.CATEGORY.ordinal -> CategoriesViewHolder.from(parent, context)
            else -> EventsViewHolder.from(parent)
        }
    }

    /**
     * Returns the viewType of each item in the list upon which the ViewHolder will be created.
     */
    override fun getItemViewType(position: Int) = getItem(position).eventListType.ordinal

    /**
     * Replaces the contents of a view (invoked by the layout manager).
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = getItem(position)

        when (holder) {
            is DatesViewHolder -> {
                holder.bind(dateListener)
            }
            is EventsViewHolder -> {
                holder.bind(item)
            }
            is CategoriesViewHolder -> {
                holder.bind(categoryListener, item.events)
            }
        }

    }

}

/**
 * Allows the [RecyclerView] to determine which items have changed when the [List] of [EventListSection] has been updated.
 */
class HomeDiffCallback : DiffUtil.ItemCallback<EventListSection>() {

    override fun areItemsTheSame(oldItem: EventListSection, newItem: EventListSection): Boolean {
        return oldItem.header == newItem.header
    }

    override fun areContentsTheSame(oldItem: EventListSection, newItem: EventListSection): Boolean {
        return oldItem == newItem
    }

}

