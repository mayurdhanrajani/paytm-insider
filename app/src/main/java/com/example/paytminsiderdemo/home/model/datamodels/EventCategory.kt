package com.example.paytminsiderdemo.home.model.datamodels

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = false)
@Parcelize
data class EventCategory(
    @Json(name = "_id") val id: String = "",
    val name: String = "",
    @Json(name = "icon_img") val iconImg: String = ""
) : Parcelable