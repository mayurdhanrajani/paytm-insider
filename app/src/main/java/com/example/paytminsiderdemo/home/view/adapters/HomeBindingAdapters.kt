package com.example.paytminsiderdemo.home.view.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.network.UIState

/**
 * This binding adapter displays the [UIState] of the network request in an [ImageView].
 * When the request is loading, it displays a loading animation.
 * If the request has an error, it displays a connection error image.
 * When the request is finished, it hides the [ImageView].
 */
@BindingAdapter("imageViewStatus")
fun showImageView(imageView: ImageView, uiState: UIState) {
    when (uiState) {
        UIState.LOADING -> {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.loading_animation)
        }
        UIState.ERROR -> {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.ic_connection_error)
        }
        UIState.EMPTY -> {
            imageView.visibility = View.VISIBLE
            imageView.setImageResource(R.drawable.ic_empty_data)
        }
        UIState.SUCCESS -> {
            imageView.visibility = View.GONE
        }
    }
}

/**
 * This binding adapter displays the [UIState] of the network request in an image view.
 * When the response is empty of has any error, it shows the [TextView] with some info else hides it.
 */
@BindingAdapter("textViewStatus")
fun showTextView(textView: TextView, uiState: UIState) {
    when (uiState) {
        UIState.LOADING -> {
            textView.visibility = View.GONE
        }
        UIState.ERROR -> {
            textView.visibility = View.VISIBLE
            textView.text = textView.context.getString(R.string.unknown_error)
        }
        UIState.EMPTY -> {
            textView.visibility = View.VISIBLE
            textView.text = textView.context.getString(R.string.no_events)
        }
        UIState.SUCCESS -> {
            textView.visibility = View.GONE
        }
    }
}

/**
 * This binding adapter displays the [UIState] of the network request in an image view.
 * When the response is success, it shows the [RecyclerView] else hides it.
 */
@BindingAdapter("recyclerViewStatus")
fun showHideRecyclerView(recyclerView: RecyclerView, uiState: UIState) {
    when (uiState) {
        UIState.SUCCESS -> {
            recyclerView.visibility = View.VISIBLE
        }
        else -> {
            recyclerView.visibility = View.GONE
        }
    }
}