package com.example.paytminsiderdemo.home.model.datamodels

import android.os.Parcelable
import androidx.lifecycle.LiveData
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = false)
@Parcelize
data class EventData(
    val list: EventList = EventList(),
    val picks: EventList = EventList(),
    val popular: List<EventDetails> = listOf(),
    val featured: List<EventDetails> = listOf()
) : Parcelable