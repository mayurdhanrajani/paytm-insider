package com.example.paytminsiderdemo.home.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.databinding.ListItemVerticalEventBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.utils.loadImage

/**
 * The VerticalEventViewHolder constructor takes the binding variable from the associated
 * ListItemVerticalEvent, which gives it access to the full [EventDetails] information.
 */
class VerticalEventViewHolder(private val binding: ListItemVerticalEventBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(eventDetails: EventDetails) {
        binding.data = eventDetails
        binding.ivEvent.loadImage(eventDetails.horizontalCoverImage)
    }

    companion object {
        fun from(parent: ViewGroup): VerticalEventViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemVerticalEventBinding.inflate(layoutInflater, parent, false)
            return VerticalEventViewHolder(
                binding
            )
        }
    }

}