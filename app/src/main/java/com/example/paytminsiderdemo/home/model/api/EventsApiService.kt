package com.example.paytminsiderdemo.home.model.api

import com.example.paytminsiderdemo.home.model.datamodels.EventData
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface that exposes the [getEvents] method
 */
interface EventsApiService {

    /**
     * Returns a Retrofit callback using GET HTTP method.
     */
    @GET("home")
    suspend fun getEvents(
        @Query("norm") norm: String,
        @Query("filterBy") filterBy: String,
        @Query("city") city: String
    ): EventData

}