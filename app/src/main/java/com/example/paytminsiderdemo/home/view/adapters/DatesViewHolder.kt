package com.example.paytminsiderdemo.home.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.databinding.ListItemDateBinding
import com.example.paytminsiderdemo.home.utils.DateListener
import com.example.paytminsiderdemo.utils.EventDateType
import com.example.paytminsiderdemo.utils.getEndTime
import com.example.paytminsiderdemo.utils.getStartTime
import java.util.*

/**
 * The DatesViewHolder constructor takes the binding variable from the associated
 * ListItemDate, which gives it access to the full [DateListener] information.
 */
class DatesViewHolder(private val binding: ListItemDateBinding) :
    RecyclerView.ViewHolder(binding.root) {

    private val calendar = Calendar.getInstance()

    fun bind(dateListener: DateListener) {

        binding.btnToday.setOnClickListener {
            dateListener.onClick(
                calendar.getStartTime(0),
                calendar.getEndTime(0),
                EventDateType.TODAY
            )
        }

        binding.btnTomorrow.setOnClickListener {
            dateListener.onClick(
                calendar.getStartTime(1),
                calendar.getEndTime(1),
                EventDateType.TOMORROW
            )
        }

        binding.btnWeekend.setOnClickListener {
            val day = when (calendar.get(Calendar.DAY_OF_WEEK)) {
                1 -> -1
                else -> (7 - calendar.get(Calendar.DAY_OF_WEEK))
            }
            dateListener.onClick(
                calendar.getStartTime(day),
                calendar.getEndTime(day + 1),
                EventDateType.WEEKEND
            )
        }

    }

    companion object {
        fun from(parent: ViewGroup): DatesViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemDateBinding.inflate(layoutInflater, parent, false)
            return DatesViewHolder(
                binding
            )
        }
    }

}