package com.example.paytminsiderdemo.home.di

import androidx.lifecycle.ViewModel
import com.example.paytminsiderdemo.di.PerFragment
import com.example.paytminsiderdemo.di.ViewModelKey
import com.example.paytminsiderdemo.home.model.api.EventsApiService
import com.example.paytminsiderdemo.home.viewmodel.HomeViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

/**
 * Provides the dependencies required for the HomeFragment.
 */
@Module
class HomeModule {

    /**
     * Provides the implementation of [EventsApiService].
     */
    @PerFragment
    @Provides
    fun provideApi(retrofit: Retrofit): EventsApiService {
        return retrofit.create(EventsApiService::class.java)
    }

    /**
     * Provides the implementation of [HomeViewModel].
     */
    @PerFragment
    @IntoMap
    @Provides
    @ViewModelKey(HomeViewModel::class)
    fun provideViewModel(viewModel: HomeViewModel): ViewModel = viewModel

}