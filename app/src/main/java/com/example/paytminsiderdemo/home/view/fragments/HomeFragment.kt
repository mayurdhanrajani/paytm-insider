package com.example.paytminsiderdemo.home.view.fragments

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.app.PaytmInsiderApp
import com.example.paytminsiderdemo.databinding.FragmentHomeBinding
import com.example.paytminsiderdemo.databinding.ViewCustomDialogBinding
import com.example.paytminsiderdemo.home.di.DaggerHomeComponent
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.home.utils.CategoryListener
import com.example.paytminsiderdemo.home.utils.DateListener
import com.example.paytminsiderdemo.home.view.adapters.HomeAdapter
import com.example.paytminsiderdemo.home.viewmodel.HomeViewModel
import com.example.paytminsiderdemo.utils.ViewModelProviderFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*
import javax.inject.Inject

/**
 * Shows the list of events.
 */
class HomeFragment : Fragment() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    private lateinit var binding: FragmentHomeBinding

    private lateinit var viewModel: HomeViewModel

    private lateinit var homeAdapter: HomeAdapter

    private lateinit var masterList: List<EventDetails>

    private lateinit var cityName: String

    /**
     * Generates the Dagger component to inject the dependencies before view is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDaggerComponent()
    }

    /**
     * Inflates the layout with Data Binding, sets its lifecycle owner to the [HomeFragment] to enable DataBinding.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    /**
     * Initializes the classes after view is created.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initializeViewModel()
    }

    /**
     * Sets up the toolbar.
     */
    private fun setupToolbar() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.apply {
            show()
            title = getString(R.string.app_name)
        }
    }

    /**
     * Sets up the [DaggerHomeComponent].
     */
    private fun setupDaggerComponent() {
        val appComponent = (activity?.application as PaytmInsiderApp).getAppComponent()
        DaggerHomeComponent.builder().appComponent(appComponent).build().inject(this)
    }

    /**
     * Initializes the ViewModel.
     */
    private fun initializeViewModel() {
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(HomeViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        observeLiveData()
    }

    /**
     * Observes the LiveData.
     */
    private fun observeLiveData() {
        viewModel.fetchEvents("")

        viewModel.sectionList.observe(viewLifecycleOwner, Observer {
            initializeAdapter()
            homeAdapter.submitList(it)
            binding.rvHome.adapter = homeAdapter
        })

        viewModel.masterList.observe(viewLifecycleOwner, Observer {
            masterList = it
        })
    }

    /**
     * Initializes the [HomeAdapter].
     */
    private fun initializeAdapter() {
        homeAdapter = HomeAdapter(

            context as Context,

            DateListener { startTime, endTime, eventDateType ->
                findNavController().apply {
                    val bundle = Bundle().apply {
                        putParcelableArrayList(getString(R.string.data), masterList as ArrayList)
                        putString(getString(R.string.filter_type), getString(R.string.date))
                        putLong(getString(R.string.start_time), startTime)
                        putLong(getString(R.string.end_time), endTime)
                        putString(getString(R.string.toolbar_title), eventDateType.toString())
                    }
                    navigate(R.id.action_homeFragment_to_eventsListFragment, bundle)
                }
            },

            CategoryListener {

                findNavController().apply {
                    val bundle = Bundle().apply {
                        putParcelableArrayList(getString(R.string.data), masterList as ArrayList)
                        putParcelable(getString(R.string.event_category), it)
                        putString(getString(R.string.filter_type), getString(R.string.category))
                        putString(getString(R.string.toolbar_title), it.name)
                    }
                    navigate(R.id.action_homeFragment_to_eventsListFragment, bundle)
                }

            })
    }

    /**
     * Inflates the overflow menu that contains filtering options.
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * menu_item_location - Updates the location in the [HomeViewModel].
     *
     * menu_item_dark_mode - Changes the theme of the app between light/dark mode.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_location -> {
                val view = DataBindingUtil.inflate<ViewCustomDialogBinding>(
                    LayoutInflater.from(context),
                    R.layout.view_custom_dialog,
                    null,
                    false
                )

                MaterialAlertDialogBuilder(activity)
                    .setTitle(getString(R.string.enter_city))
                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                        cityName = view.etCity.text.toString().toLowerCase()
                        viewModel.fetchEvents(cityName)
                    }
                    .setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
                    .setView(view.root)
                    .show()
            }

            R.id.menu_item_dark_mode -> {
                val currentNightMode =
                    resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
                setDefaultNightMode(
                    when (currentNightMode) {
                        Configuration.UI_MODE_NIGHT_NO -> MODE_NIGHT_YES
                        Configuration.UI_MODE_NIGHT_YES -> MODE_NIGHT_NO
                        else -> MODE_NIGHT_FOLLOW_SYSTEM
                    }
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

}