package com.example.paytminsiderdemo.home.model.datamodels

import com.example.paytminsiderdemo.home.utils.EventListItemType

data class EventListSection(
    var eventListType: EventListItemType,
    var events: List<EventDetails> = listOf(),
    var header: String = ""
)