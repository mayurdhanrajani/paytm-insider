package com.example.paytminsiderdemo.home.utils

import com.example.paytminsiderdemo.utils.EventDateType

/**
 * Custom listener that handles button [onClick] function.
 *
 * @param dateListener - lambda that will be called & pass the startTime, endTime and [EventDateType] of that button.
 *
 */
class DateListener(val dateListener: (startTime: Long, endTime: Long, eventDateType: EventDateType) -> Unit) {
    fun onClick(startTime: Long, endTime: Long, eventDateType: EventDateType) =
        dateListener(startTime, endTime, eventDateType)
}