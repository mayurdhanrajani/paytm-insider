package com.example.paytminsiderdemo.home.di

import com.example.paytminsiderdemo.di.AppComponent
import com.example.paytminsiderdemo.di.PerFragment
import com.example.paytminsiderdemo.home.view.fragments.HomeFragment
import dagger.Component

/**
 * Gets all the dependencies from [AppComponent] and [HomeModule].
 */
@PerFragment
@Component(dependencies = [AppComponent::class], modules = [HomeModule::class])
interface HomeComponent {
    /**
     * The injector method for injecting dependencies in [HomeFragment].
     *
     * @param homeFragment - The fragment where the dependencies are to be injected.
     */
    fun inject(homeFragment: HomeFragment)
}