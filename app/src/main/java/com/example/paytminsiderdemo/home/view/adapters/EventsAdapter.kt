package com.example.paytminsiderdemo.home.view.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.home.utils.EventListItemType

/**
 * This class implements a [RecyclerView] [ListAdapter] which uses Data Binding to present [List] data, including computing diffs between lists.
 *
 * @param eventListItemType - Based on the value of the enum class, [RecyclerView] will create that [RecyclerView.ViewHolder].
 *
 */
class EventsAdapter(private val eventListItemType: EventListItemType) :
    ListAdapter<EventDetails, RecyclerView.ViewHolder>(EventsDiffCallback()) {

    /**
     * Create new [RecyclerView] item views (invoked by the layout manager).
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (eventListItemType) {
            EventListItemType.VERTICAL_EVENT -> VerticalEventViewHolder.from(
                parent
            )
            else -> HorizontalEventViewHolder.from(
                parent
            )
        }
    }

    /**
     * Replaces the contents of a view (invoked by the layout manager).
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)

        when (holder) {
            is HorizontalEventViewHolder -> {
                holder.bind(item)
            }
            is VerticalEventViewHolder -> {
                holder.bind(item)
            }
        }
    }

}

/**
 * Allows the [RecyclerView] to determine which items have changed when the [List] of [EventDetails] has been updated.
 */
class EventsDiffCallback : DiffUtil.ItemCallback<EventDetails>() {

    override fun areItemsTheSame(oldItem: EventDetails, newItem: EventDetails): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EventDetails, newItem: EventDetails): Boolean {
        return oldItem == newItem
    }

}
