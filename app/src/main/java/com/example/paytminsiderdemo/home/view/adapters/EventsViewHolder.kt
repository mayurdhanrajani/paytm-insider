package com.example.paytminsiderdemo.home.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.paytminsiderdemo.databinding.ListItemRecyclerviewBinding
import com.example.paytminsiderdemo.home.model.datamodels.EventListSection

/**
 * The EventsViewHolder constructor takes the binding variable from the associated
 * ListItemRecyclerview, which gives it access to the full [EventListSection] information.
 */
class EventsViewHolder(private val binding: ListItemRecyclerviewBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(eventListSection: EventListSection) {
        binding.tvHeader.text = eventListSection.header

        val eventsAdapter =
            EventsAdapter(
                eventListSection.eventListType
            )
        eventsAdapter.submitList(eventListSection.events)

        binding.rvHorizontal.apply {
            adapter = eventsAdapter
        }
    }

    companion object {
        fun from(parent: ViewGroup): EventsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemRecyclerviewBinding.inflate(layoutInflater, parent, false)
            binding.rvHorizontal.apply {
                setRecycledViewPool(RecyclerView.RecycledViewPool())
                layoutManager = LinearLayoutManager(parent.context, LinearLayoutManager.HORIZONTAL, false)
            }
            return EventsViewHolder(
                binding
            )
        }
    }

}