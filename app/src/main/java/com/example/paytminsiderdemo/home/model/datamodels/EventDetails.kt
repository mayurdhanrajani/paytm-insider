package com.example.paytminsiderdemo.home.model.datamodels

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = false)
@Parcelize
data class EventDetails(
    @Json(name = "_id") val id: String = "",
    @Json(name = "min_show_start_time") val minShowStartTime: Int = -1,
    val name: String = "",
    val title: String = "",
    val type: String = "",
    @Json(name = "horizontal_cover_image") val horizontalCoverImage: String = "",
    @Json(name = "vertical_cover_image") val verticalCoverImage: String = "",
    val city: String = "",
    @Json(name = "venue_id") val venueId: String = "",
    @Json(name = "venue_name") val venueName: String = "",
    @Json(name = "venue_date_string") val venueDateString: String = "",
    @Json(name = "category_id") val eventCategory: EventCategory = EventCategory(),
    @Json(name = "event_state") val eventState: String = "",
    @Json(name = "price_display_string") val priceDisplayString: String = ""
) : Parcelable