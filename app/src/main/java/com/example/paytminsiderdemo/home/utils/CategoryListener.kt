package com.example.paytminsiderdemo.home.utils

import com.example.paytminsiderdemo.home.model.datamodels.EventCategory

/**
 * Custom listener that handles clicks on RecyclerView items.
 *
 * @param categoryListener - lambda that will be called & pass the current [EventCategory]
 *
 */
class CategoryListener(val categoryListener: (eventCategory: EventCategory) -> Unit) {
    fun onClick(eventCategory: EventCategory) = categoryListener(eventCategory)
}