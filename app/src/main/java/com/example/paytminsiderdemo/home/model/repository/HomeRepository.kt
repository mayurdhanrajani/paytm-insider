package com.example.paytminsiderdemo.home.model.repository

import com.example.paytminsiderdemo.home.model.api.EventsApiService
import com.example.paytminsiderdemo.network.NetworkResponseState
import com.example.paytminsiderdemo.network.executeCoroutine
import javax.inject.Inject

/**
 * Class which handles the data operations.
 *
 * @param eventsApiService - Instance of [EventsApiService] using @Inject annotation.
 *
 */
class HomeRepository @Inject constructor(private val eventsApiService: EventsApiService) {

    /**
     * Fetches the events using coroutines.
     */
    suspend fun fetchEvents(cityName: String): NetworkResponseState {
        return executeCoroutine { eventsApiService.getEvents("1", "go-out", cityName) }
    }

}
