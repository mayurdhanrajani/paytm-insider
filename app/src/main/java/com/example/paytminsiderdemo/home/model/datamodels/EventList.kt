package com.example.paytminsiderdemo.home.model.datamodels

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = false)
@Parcelize
data class EventList(
    @Json(name = "masterList") val events: Map<String, EventDetails> = mapOf()
): Parcelable