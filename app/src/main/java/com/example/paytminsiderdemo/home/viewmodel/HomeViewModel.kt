package com.example.paytminsiderdemo.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.paytminsiderdemo.home.model.datamodels.EventData
import com.example.paytminsiderdemo.home.model.datamodels.EventDetails
import com.example.paytminsiderdemo.home.model.datamodels.EventListSection
import com.example.paytminsiderdemo.home.model.repository.HomeRepository
import com.example.paytminsiderdemo.home.utils.EventListItemType
import com.example.paytminsiderdemo.network.NetworkResponseState
import com.example.paytminsiderdemo.network.UIState
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * The [ViewModel] that is attached to the HomeFragment.
 *
 * @param homeRepository - Instance of [HomeRepository] using @Inject annotation to fetch the events.
 *
 */
class HomeViewModel @Inject constructor(private val homeRepository: HomeRepository) : ViewModel() {

    private val _sectionList = MutableLiveData<List<EventListSection>>()
    val sectionList: LiveData<List<EventListSection>> = _sectionList

    private val _masterList = MutableLiveData<List<EventDetails>>()
    val masterList: LiveData<List<EventDetails>> = _masterList

    private val _recyclerViewStatus = MutableLiveData<UIState>()
    val recyclerViewStatus: LiveData<UIState> = _recyclerViewStatus

    private val _imageViewStatus = MutableLiveData<UIState>()
    val imageViewStatus: LiveData<UIState> = _imageViewStatus

    private val _textViewStatus = MutableLiveData<UIState>()
    val textViewStatus: LiveData<UIState> = _textViewStatus

    /**
     * Gets events list from the Events API Retrofit service and updates the [EventDetails] [List] and [UIState] [LiveData].
     *
     * @param cityName - The name of the city that is sent as part of the request.
     *
     */
    fun fetchEvents(cityName: String) {
        setUIState(UIState.LOADING)

        viewModelScope.launch {

            when (val networkResponseState = homeRepository.fetchEvents(cityName)) {

                is NetworkResponseState.Success -> {
                    setUIState(UIState.SUCCESS)
                    val response = networkResponseState.data
                    _masterList.value = response.list.events.map { it.value }
                    _sectionList.value = createSectionList(response)
                }

                is NetworkResponseState.ApiError -> {
                    initializeEmptyList()
                    setUIState(UIState.EMPTY)
                }

                is NetworkResponseState.NetworkError -> {
                    initializeEmptyList()
                    setUIState(UIState.ERROR)
                }

                is NetworkResponseState.UnknownError -> {
                    initializeEmptyList()
                    setUIState(UIState.ERROR)
                }

            }

        }
    }

    /**
     * Initializes the list if the request is not successful.
     */
    private fun initializeEmptyList() {
        _masterList.value = listOf()
        _sectionList.value = listOf()
    }

    /**
     * Adds the events to the [EventListSection] along with header and [EventListItemType].
     */
    private fun createSectionList(response: EventData): MutableList<EventListSection> {
        val eventsSectionList = mutableListOf<EventListSection>()

        eventsSectionList.add(EventListSection(eventListType = EventListItemType.DATE))

        if (response.featured.isNotEmpty()) {
            eventsSectionList.add(
                EventListSection(
                    eventListType = EventListItemType.HORIZONTAL_EVENT,
                    events = response.featured,
                    header = "Featured Events"
                )
            )
        }

        if (response.picks.events.isNotEmpty()) {
            eventsSectionList.add(
                EventListSection(
                    eventListType = EventListItemType.VERTICAL_EVENT,
                    events = response.picks.events.map { it.value },
                    header = "Top Picks For You"
                )
            )
        }

        if (response.popular.isNotEmpty()) {
            eventsSectionList.add(
                EventListSection(
                    eventListType = EventListItemType.HORIZONTAL_EVENT,
                    events = response.popular,
                    header = "Popular Events"
                )
            )
        }

        eventsSectionList.add(
            EventListSection(
                eventListType = EventListItemType.CATEGORY,
                events = (masterList.value ?: listOf()).distinctBy { it.eventCategory.id },
                header = "Categories"
            )
        )

        return eventsSectionList
    }

    /**
     * Sets the [UIState] value to different views.
     */
    private fun setUIState(uiState: UIState) {
        _recyclerViewStatus.value = uiState
        _imageViewStatus.value = uiState
        _textViewStatus.value = uiState
    }

}