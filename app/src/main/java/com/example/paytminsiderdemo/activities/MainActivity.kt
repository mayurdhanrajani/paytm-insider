package com.example.paytminsiderdemo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.paytminsiderdemo.R
import com.example.paytminsiderdemo.databinding.ActivityMainBinding

/**
 * The launcher activity which is called when the app starts.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    /**
     * Binds the layout using data binding and setup the Navigation Controller.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupNavController()
    }

    /**
     * Setup the Navigation Controller.
     */
    private fun setupNavController() {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        setSupportActionBar(binding.appToolbar)

        val appBarConfiguration = AppBarConfiguration
            .Builder(
                R.id.splashFragment,
                R.id.homeFragment
            )
            .build()

        NavigationUI.setupWithNavController(
            binding.appToolbar,
            navController,
            appBarConfiguration
        )
    }

}
